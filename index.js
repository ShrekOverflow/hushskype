const fs = require('fs');
const sysPath = require('path');
const skypeHttp = require('skype-http');
const psNode = require('ps-node');
const {createInterface} = require('readline');

const LIST_OF_PROCESSESS = [
  // Add lowercase process names 
  // for example if you want to ban
  // visual studio code add 
  // 'vscode'
];


function promptCredentials() {
  return new Promise((resolve, reject) => {

    const cliInterface = createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    cliInterface.on('error', reject);
    cliInterface.question('Username? ', function (username) {
      cliInterface.question('Password? ', function (password) {
        cliInterface.close();
        resolve({username, password});
      });
    });
  });

}

function login(){
  const statePath = sysPath.resolve(__dirname, "api-state.json");
  try {
    const stateContent = fs.readFileSync(statePath).toString("utf8");
    const apiContext = JSON.parse(stateContent);
    return skypeHttp.connect({
      state: apiContext,
      verbose: false
    });

  } catch (err) {
    return promptCredentials()
      .then((credentials) => skypeHttp.connect({
        credentials: credentials,
        verbose: false
      }));
  }

}

function psAsync(params={}) {
  return new Promise(function (resolve, reject){
    psNode.lookup(params, function (err, list){
      if(err) {
        console.error(err);
        reject(err);
      }
      resolve(list);
    });
  });
}

function step(){
  return psAsync()
    .then((processes) => {
      for (let proc of processes) {
        if (LIST_OF_PROCESSESS.some((listProc) => proc.command.toLowerCase().indexOf(listProc) !== -1)) {
          return true;
        }
      }
      return false;
    });
}

function run() {
  return login().then((api) => {
    const statePath = sysPath.resolve(__dirname, "api-state.json");
    console.log(api.getState, api.setStatus);
    // This does not exist in the package, created bug will fix later.
    // fs.writeFileSync(statePath, JSON.stringify(api.getState()), 'utf8');
    return api;
  }).then((api) => {
    // Log every error
    console.log("Ready to monitor and set you busy if either of the following processes are running \n", LIST_OF_PROCESSESS.join('\n'));
    api.on("error", (err) => {
      console.error("An error was detected:");
      console.error(err);
    });

    setInterval(function () {
      step().then(isBusy => {
        if (isBusy) {
          api.setStatus("Busy");
        }
      });
    }, 30000);
  });

}

run()
  .catch((err) => {
    console.error(err.stack);
    return process.exit(1);
  });